import { Logger } from 'pino';
import { setupGlobalErrorHandler } from '../src/errorHandler';

jest.mock('pino', () => ({
  Logger: jest.fn()
}));

describe('setupGlobalErrorHandler', () => {
  let mockLogger: Partial<Logger>;

  beforeEach(() => {
    mockLogger = {
      error: jest.fn()
    };
    window.onerror = jest.fn();
    setupGlobalErrorHandler(mockLogger as Logger);
  });

  it('should set global error handlers', () => {
    expect(window.onerror).toBeInstanceOf(Function);
  });

  it('should log errors via onerror', () => {
    const errorMessage = 'Test error';
    const filename = 'test.js';
    const lineno = 100;
    const colno = 10;
    const error = new Error('Error for testing');

    window.onerror!(errorMessage, filename, lineno, colno, error);

    expect(mockLogger.error).toHaveBeenCalledWith({
      message: errorMessage,
      source: filename,
      lineno: lineno,
      colno: colno,
      error: error.stack || error
    });
  });
});
