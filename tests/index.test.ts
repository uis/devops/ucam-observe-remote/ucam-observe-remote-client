import {
  getPinoRemoteLogger,
  LoggerOptions,
  LogLevel,
  pinoToBackendLogLevelMap
} from '../src/index';
import { Logger } from 'pino';

describe('LogLevel definitions', () => {
  it('should have defined log levels', () => {
    const levels: LogLevel[] = [
      'info',
      'error',
      'debug',
      'warning',
      'fatal',
      'critical',
      'notset'
    ];
    expect(levels).toContain('info');
    expect(levels).toContain('error');
    expect(levels).toContain('debug');
    expect(levels).toContain('warning');
    expect(levels).toContain('fatal');
    expect(levels).toContain('critical');
    expect(levels).toContain('notset');
  });
});

describe('LoggerOptions interface', () => {
  it('should allow creation of a LoggerOptions object', () => {
    const options: LoggerOptions = {
      level: 'info',
      remoteUrl: 'http://example.com/log'
    };
    expect(options.level).toEqual('info');
    expect(options.remoteUrl).toEqual('http://example.com/log');
    expect(options.clientId).not.toBeDefined();
  });

  it('should allow creation of a LoggerOptions object with a client id', () => {
    const options: LoggerOptions = {
      level: 'info',
      remoteUrl: 'http://example.com/log',
      clientId: 'my-client'
    };
    expect(options.level).toEqual('info');
    expect(options.remoteUrl).toEqual('http://example.com/log');
    expect(options.clientId).toEqual('my-client');
  });
});

describe('pinoToBackendLogLevelMap correctness', () => {
  it('should correctly map numeric keys to LogLevel values', () => {
    expect(pinoToBackendLogLevelMap['60']).toEqual('critical');
    expect(pinoToBackendLogLevelMap['50']).toEqual('error');
    expect(pinoToBackendLogLevelMap['40']).toEqual('warning');
    expect(pinoToBackendLogLevelMap['30']).toEqual('info');
    expect(pinoToBackendLogLevelMap['20']).toEqual('debug');
    expect(pinoToBackendLogLevelMap['10']).toEqual('notset');
  });
});

describe('getPinoRemoteLogger', () => {
  const stdoutSpy = jest.spyOn(process.stdout, 'write').mockImplementation(() => true);

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should return a Logger object', () => {
    const options: LoggerOptions = {
      level: 'info',
      remoteUrl: 'http://example.com/log'
    };
    const logger: Logger = getPinoRemoteLogger(options);
    expect(logger).toBeDefined();
  });
  it('should have the correct log level', () => {
    const options: LoggerOptions = {
      level: 'info',
      remoteUrl: 'http://example.com/log'
    };
    const logger: Logger = getPinoRemoteLogger(options);
    expect(logger.level).toEqual('info');
  });

  it('should log to console', () => {
    const options: LoggerOptions = {
      level: 'info',
      remoteUrl: 'http://example.com/log'
    };
    const logger: Logger = getPinoRemoteLogger(options);
    logger.info('This is an info message');
    expect(stdoutSpy).toHaveBeenCalled();
    expect(stdoutSpy.mock.calls[0][0]).toContain('This is an info message');
  });

  it('should handle errors correctly', () => {
    const options: LoggerOptions = {
      level: 'error',
      remoteUrl: 'http://example.com/log'
    };
    const logger: Logger = getPinoRemoteLogger(options);
    logger.error('This is an error message');
    expect(stdoutSpy).toHaveBeenCalled();
    expect(stdoutSpy.mock.calls[0][0]).toContain('This is an error message');
  });
});
