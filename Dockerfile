# Start from a Debian image so that we can install Java. This is required for
# openapitools.
FROM node:18-buster-slim as development

WORKDIR /usr/src/app

# Install Java
RUN set -ex ; \
    apt-get -y update ; \
    apt-get install -y curl openjdk-11-jre-headless ; \
    apt-get clean -y

# Install Cypress dependencies for development
RUN apt-get install -y \
    libgtk2.0-0 \
    libgtk-3-0 \
    libgbm-dev \
    libnotify-dev \
    libgconf-2-4 \
    libnss3 \
    libxss1 \
    libasound2 \
    libxtst6 \
    xauth \
    xvfb

# create and change ownership as we mount a volume at this location
# and want the permissions to persist
RUN mkdir /usr/src/app/node_modules/
RUN chown -R node:node /usr/src/app/

USER node
FROM development as builder

COPY package*.json ./

# run the build
RUN npm ci

# copy the rest of the source code and tsconfig for compilation
COPY tsconfig.json .
COPY src src

# run the build
RUN npm run build

FROM nginx:mainline-alpine as production

WORKDIR /usr/src/app

# Remember to update to grab security fixes
RUN apk update && apk upgrade && apk add jq

# default the port to 8000 - this can be overriden at invoke-time
ENV PORT=8000